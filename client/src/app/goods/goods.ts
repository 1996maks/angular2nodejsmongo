import {Component} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http, Headers} from "@angular/http";
import {Services} from '../services/services';
import myGlobals = require('../global'); //<==== this one

@Component({
  selector: 'goods',
  template: require('./goods.html')
})

export class Goods {

private id: any;
// private data: any;
private goods: any;
private comments: any;
private sub: any;
text : string;
  list_of_goods:string;

  constructor(private route: ActivatedRoute, private http: Http, private services:Services) {
    this.services.getGoods()
    .subscribe(goods=> {
    this.goods = goods;
     console.log(goods);
      });


      this.services.getComments()
      .subscribe(comments=>{
        this.comments = comments;
        console.log(comments);
      });
    }


buyGood(item){
//document.getElementById("basket").style.color = "white";

localStorage.setItem(item._id, item.name);
window.location.reload();

}
addComment(event){
  event.preventDefault();

  var commentar={
    text: this.text
  }
  
this.services.addComment(commentar)
.subscribe(comments=>{

  this.comments.push(commentar);
  this.text = "";
});
}

deleteComment(id){
  
  var comments = this.comments;

  this.services.deleteComment(id).subscribe(data=>{
    if(data.n == 1){
      for(var i =0; i<comments.length;i++){
        if(comments[i].id==id){
          comments.splice(i, 1);
        }
      }
    }
  }); 
  window.location.reload();
}


 private ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      console.log(this.id);
      
      
    });

    var token = localStorage.getItem('token');
  }

}
