import {Goods} from './goods';
import {TestBed, async} from '@angular/core/testing';

describe('goods component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({declarations: [Goods]});
    TestBed.compileComponents();
  }));

  it('should render...', () => {
    const fixture = TestBed.createComponent(Goods);
    fixture.detectChanges();
  });
});
