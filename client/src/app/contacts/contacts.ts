import {Component, NgModule, Input} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {Http} from '@angular/http';
import {NgForm} from '@angular/forms';
import 'rxjs';


export interface ContactList{
 name:string;
 last_name:string;
 telephone_number:string;
}



@Component({
  selector: 'contacts',
  template: require('./contacts.html')
})


export class Contacts {


onSubmit(form: NgForm){
  console.log(form);
  
}


lat: number = 49.228788;
lng: number = 28.483867;

 contacts: ContactList[] = [
{
    "name":"Maxim",
    "last_name": "Musiichuk",
    "telephone_number":"0632754682"
  },
 {
    "name":"Petya",
    "last_name": "Pupkin",
    "telephone_number":"0992457896"
  },{
    "name":"Vasya",
    "last_name": "Bariban",
    "telephone_number":"0736589741"
  },{
    "name":"Masha",
    "last_name": "Mihaylenko",
    "telephone_number":"0936547896"
  },{
    "name":"Alyona",
    "last_name": "Malevanaya",
    "telephone_number":"0965478956"
  },{
    "name":"Alisa",
    "last_name": "Pistar",
    "telephone_number":"0779654782"
  }
]



}