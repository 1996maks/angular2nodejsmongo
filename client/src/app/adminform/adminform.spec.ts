import {Adminform} from './adminform';
import {TestBed, async} from '@angular/core/testing';

describe('adminform component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({declarations: [Adminform]});
    TestBed.compileComponents();
  }));

  it('should render...', () => {
    const fixture = TestBed.createComponent(Adminform);
    fixture.detectChanges();
  });
});
