import { Component, NgZone} from '@angular/core';
import { Http, RequestOptions } from "@angular/http";
import { Services } from "../services/services";
import {Headers} from '@angular/http';
import myGlobals = require('../global'); //<==== this one
import { Observable } from "rxjs/Observable";
import 'rxjs/Rx'; 
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as XLSX from 'ts-xlsx';

import { Ng2DragDropModule } from 'ng2-drag-drop';



// import { Ng2ImgMaxService } from 'ng2-img-max';
// import { Ng2ImgToolsService } from 'ng2-img-tools';
// import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

@Component({
  selector: 'adminform',
  template: require('./adminform.html')
})


export class Adminform {
 

  includeColumnHeader: any;
  uploadFile: any;
    
  private file
  private goods: any;
  filesToUpload: Array<File>;

 private changedName: any;

  name : string;
  diagonal: string;
  razresheniye:string;
  memory:string;
  camera:string;
  processor:string;
  description:string;
  price:string;
  path:string;
  resizeImg:string;
  private fileName:any;
  private telephones: any;
 
 
constructor(private http: Http, private services:Services) {
    this.services.getGoods()
    .subscribe(goods=> {
    this.goods = goods;
     console.log(goods);
     
      });

    this.filesToUpload = [];
    
  }
   
upload() {
  
        this.makeFileRequest("http://localhost:3002/api/profile", [], this.filesToUpload).then((result) => {
            console.log(result);
        }, (error) => {
            console.error(error);
        });
    }
 
fileChangeEvent(fileInput: any){
        this.filesToUpload = <Array<File>> fileInput.target.files;

    }
 
makeFileRequest(url: string, params: Array<string>, files: Array<File>) {
        return new Promise((resolve, reject) => {
            var formData: any = new FormData();
            var xhr = new XMLHttpRequest();
            for(var i = 0; i < files.length; i++) {
                formData.append("good_picture", files[i], files[i].name);
            }
            xhr.onreadystatechange = function () {
                if (xhr.readyState == 4) {
                    if (xhr.status == 200) {
                        resolve(JSON.parse(xhr.response));
                    } else {
                        reject(xhr.response);
                    }
                }
            }
            xhr.open("POST", url, true);
            xhr.send(formData);
        });
    }


addTelephone(){
  
    var telephone = {

      name: this.name,
      diagonal: this.diagonal,
      razresheniye:this.razresheniye,
      memory:this.memory,
      camera:this.camera,
      processor:this.processor,
      description:this.description,
      price:this.price,
      path: 'http://localhost:3002/images/'+this.filesToUpload[0].name,
      resizeImg: 'http://localhost:3002/images/resize/'+this.filesToUpload[0].name
    }
    console.log(this.uploadFile);
    console.log(telephone);
    this.upload();
    this.services.addTelephone(telephone)
    .subscribe(telephones=>{
      console.log(telephone);
      window.location.reload();
    });

}

deleteGood(id){
  
  var goods = this.goods;

  this.services.deleteGoods(id).subscribe(data=>{
    if(data.n == 1){
      for(var i =0; i<goods.length;i++){
        if(goods[i].id==id){
          goods.splice(i, 1);
        }
      }
    }
  }); 
  window.location.reload();
}

public clickedItem = {_id:"",name: "",diagonal:"", razresheniye:"",memory:"",camera:"",processor:"",description:"",path:"",price:""};
     onItemClicked(goods) {
        this.clickedItem = goods;
     }


updateGood(){
  console.log(this.clickedItem._id+ this.clickedItem.name);

    var good = {
      id:this.clickedItem._id,
      changedName:this.clickedItem.name,
      diagonal:this.clickedItem.diagonal,
      razresheniye:this.clickedItem.razresheniye,
      memory:this.clickedItem.memory,
      camera:this.clickedItem.camera,
      processor:this.clickedItem.processor,
      description:this.clickedItem.description,
      price:this.clickedItem.price,
      path_images:this.clickedItem.path
    }
    
    this.services.updateGood(good,this.clickedItem._id).subscribe(data=>{
      console.log(this.changedName);
    })
}




}
