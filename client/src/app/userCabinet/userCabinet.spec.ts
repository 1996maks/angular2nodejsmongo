import {UserCabinet} from './userCabinet';
import {TestBed, async} from '@angular/core/testing';

describe('userCabinet component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({declarations: [UserCabinet]});
    TestBed.compileComponents();
  }));

  it('should render...', () => {
    const fixture = TestBed.createComponent(UserCabinet);
    fixture.detectChanges();
  });
});
