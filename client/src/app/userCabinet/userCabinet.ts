import {Component} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http, Headers} from "@angular/http";
import {Services} from '../services/services';
import myGlobals = require('../global'); //<==== this one
import { Angular2Csv } from 'angular2-csv/Angular2-csv';
import * as XLSX from 'ts-xlsx';

@Component({
  selector: 'userCabinet',
  template: require('./userCabinet.html')
})
export class UserCabinet {
  orders: any;
  surname: any;
  email: any;
  telephone: any;
  private sub: any;
  private id: any;
  name: string;
  users: any;
  public text: string;

  constructor(private route: ActivatedRoute, private http: Http, private services:Services) {
    this.text = localStorage.getItem("username");

     this.services.getUsers()
    .subscribe(users=> {
    this.users = users;
     console.log(users);
      });


      this.services.getOrders()
    .subscribe(orders=> {
     this.orders = orders;
     console.log(this.orders);
     
      });
  }


  private ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this.id = params['id']; // (+) converts string 'id' to a number
      console.log(this.id);
      
      
    });

    console.log("aaaaaaaaaaaaaaaa" + this.name);

  }

  public clickedItem = {_id:"",name: "",surname:"", email:"",telephone:"",password:""};
     onItemClicked(users) {
        this.clickedItem = users;
     }


downloadCsv(){
var options = { 
    fieldSeparator: ',       ',
    quoteStrings: '"',
    decimalseparator: '.',
    showLabels: true, 
    showTitle: true 
  };
new Angular2Csv(this.orders, 'Товары', options );  
}

  updateUser(){

    var user = {
      name:this.clickedItem.name,
      surname:this.clickedItem.surname,
      password:this.clickedItem.password,
      email:this.clickedItem.email,
      telephone:this.clickedItem.telephone
    } 
    localStorage.setItem('username', this.clickedItem.name);
    this.services.updateUser(user,this.clickedItem._id).subscribe(data=>{
      console.log(this.clickedItem._id);
    })
}
  
}
