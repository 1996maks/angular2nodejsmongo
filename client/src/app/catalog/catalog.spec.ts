import {Catalog} from './catalog';
import {TestBed, async} from '@angular/core/testing';

describe('catalog component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({declarations: [Catalog]});
    TestBed.compileComponents();
  }));

  it('should render...', () => {
    const fixture = TestBed.createComponent(Catalog);
    fixture.detectChanges();
  });
});
