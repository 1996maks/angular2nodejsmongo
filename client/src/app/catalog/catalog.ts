import {Component} from '@angular/core';
import {Http, Jsonp, Response, Headers, RequestOptions } from '@angular/http';
import {Observable} from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import myGlobals = require('../global'); //<==== this one
import { CompleterService, CompleterData } from 'ng2-completer';
import { Services } from "../services/services";
import 'rxjs/Rx';
import { Ng2CompleterModule } from "ng2-completer";
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from "@angular/forms";
import { RouterModule, RouterLink, Routes, Router } from '@angular/router';


// import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
// import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
// import { NguiAutoCompleteModule } from '@ngui/auto-complete';

@Component({
  selector: 'catalog',
  template: require('./catalog.html')
})


export class Catalog {
  single: any;
  singleGood: any;

  protected searchStr: string;
  protected captain: string;
  protected dataService: CompleterData;

constructor(private http: Http, private completerService: CompleterService, private services:Services,private router: Router) { 
    this.services.getGoods()
    .subscribe(goods=> {
     this.goods = goods;
     console.log(goods);
         
         this.dataService = completerService.local(goods, 'name', 'name').imageField("resizeImg");
       
     
      });
      
  
  }

   ngOnInit(): void {
      let goods = this.getGoods();
     
      }

search(){
  console.log(this.searchStr);
  
  for(var i = 0; i < this.goods.length;i++){
    if(this.searchStr==this.goods[i].name){
      console.log(this.goods[i]._id);
      this.router.navigate(['goods/',this.goods[i]._id ]);
    }
  }
 
}

private goods: any;

getGoods(){
  return this.http.get( myGlobals.server_name + '/api/goods')
  .map(res => res.json())
  .subscribe(goods=> {
    this.goods = goods;
     console.log(goods);
 }, (rej) => {console.error("Could not load local data",rej)});
}


}