import {Services} from './services';
import {TestBed, async} from '@angular/core/testing';

describe('services component', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({declarations: [Services]});
    TestBed.compileComponents();
  }));

  it('should render...', () => {
    const fixture = TestBed.createComponent(Services);
    fixture.detectChanges();
  });
});
