//import {Component} from '@angular/core';
import { Injectable} from '@angular/core';
import { Http, Headers} from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/add/operator/map';
import myGlobals = require('../global'); //<==== this one

// @Component({
//   selector: 'services',
//   template: require('./services.html')
// })

@Injectable()
export class Services {
  token: any;

  constructor(private http:Http, private route: ActivatedRoute){
    console.log('Service is working');
     this.token = localStorage.getItem('token');
     console.log('Service is working' + this.token);
  }

 

getGoods(){
  return this.http.get(myGlobals.server_name +'/api/goods')
  .map(res => res.json());
  }
getComments(){
  return this.http.get(myGlobals.server_name +'/api/comments')
  .map(res => res.json());
}
getUsers(){
  return this.http.get(myGlobals.server_name +'/api/users')
  .map(res => res.json());
}
addComment(commentar){
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  return this.http.post(myGlobals.server_name +'/api/comments', JSON.stringify(commentar), {headers:headers})
  .map(res => res.json());
}


deleteComment(id){
return this.http.delete(myGlobals.server_name +'/api/comments/'+id)
  .map(res => res.json());
}

deleteGoods(id){  
return this.http.delete(myGlobals.server_name +'/api/goods/'+id)
  .map(res => res.json());
}

updateGood(good,id){
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  // return this.http.put(myGlobals.server_name + '/api/goods/' + id, JSON.stringify(good),{headers:headers})
  return this.http.put(myGlobals.server_name + '/api/goods/' + id, good)
  .map(res => res.json());
}
updateUser(user,id){
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  // return this.http.put(myGlobals.server_name + '/api/goods/' + id, JSON.stringify(good),{headers:headers})
  return this.http.put(myGlobals.server_name + '/api/users/' + id, user)
  .map(res => res.json());
}

getSingleUser(user,id){
   var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  // return this.http.put(myGlobals.server_name + '/api/goods/' + id, JSON.stringify(good),{headers:headers})
  return this.http.get(myGlobals.server_name + '/api/users/' + id, user)
  .map(res => res.json());
}
addTelephone(telephone){
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  headers.append('My-Token', this.token);
  return this.http.post(myGlobals.server_name +'/api/goods', JSON.stringify(telephone), {headers:headers})
  .map(res => res.json());
}

addUser(user){
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  return this.http.post(myGlobals.server_name +'/api/users', JSON.stringify(user), {headers:headers})
  .map(res => res.json());
}

addOrder(order){
  var headers = new Headers();
  headers.append('Content-Type', 'application/json');
  return this.http.post(myGlobals.server_name +'/api/orders', JSON.stringify(order), {headers:headers})
  .map(res => res.json());
}
getOrders(){
  return this.http.get(myGlobals.server_name +'/api/orders')
  .map(res => res.json());
}

}
