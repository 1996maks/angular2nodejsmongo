import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {routing, RootComponent} from './routes';
import {HttpModule } from '@angular/http';
import {RouterModule, Routes, RouterOutlet } from '@angular/router';
import {HelloComponent} from './hello';
import {FormsModule} from '@angular/forms';
import {Contacts} from './contacts/contacts';
import {Catalog} from './catalog/catalog';
import {Goods} from './goods/goods';
import {Adminform} from './adminform/adminform';
import {UserCabinet} from './userCabinet/userCabinet';
import {AgmCoreModule } from 'angular2-google-maps/core';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module
import {Services} from './services/services';
import { Ng2DragDropModule } from 'ng2-drag-drop';
import { Ng2CompleterModule } from "ng2-completer";
//  import { NguiAutoCompleteModule } from '@ngui/auto-complete';


// import { Ng2ImgMaxModule } from 'ng2-img-max'; // <-- import the module
// import { Ng2ImgToolsModule  } from 'ng2-img-tools'
// import { Ng2FileInputModule } from 'ng2-file-input';

@NgModule({
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    NgxPaginationModule,
    Ng2DragDropModule,
    Ng2CompleterModule,
    // Ng2ImgMaxModule,
    // Ng2FileInputModule.forRoot(),
    // Ng2ImgToolsModule,
    // NguiAutoCompleteModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyBOg4jgCEjugHzNjq-14jh4Zii0maeUF7U'
    })
  ],
  declarations: [
    RootComponent,
    HelloComponent,
    Contacts,
    Catalog,
    Goods,
    Adminform,
    UserCabinet
  ],
  providers:[Services],
  bootstrap: [RootComponent]
})


export class AppModule {}
