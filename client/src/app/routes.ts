import {Component, ModuleWithProviders } from '@angular/core';
import { RouterModule, RouterLink, Routes, Router } from '@angular/router';
import { NgForm, FormGroup } from '@angular/forms';
import { NgModule } from '@angular/core';
import {Http, Jsonp, Response, Headers, RequestOptions } from '@angular/http';
import {HelloComponent} from './hello';
import {Contacts} from './contacts/contacts';
import {Catalog} from './catalog/catalog';
import {Goods} from './goods/goods';
import {Adminform} from './adminform/adminform';
import {UserCabinet } from "./userCabinet/userCabinet";
import myGlobals = require('./global'); //<==== this one
import {Services} from './services/services';
import {Md5} from 'ts-md5/dist/md5';
declare var $:any;

import 'rxjs/add/operator/map';

'use strict';

@Component({
  selector: 'fountain-root',
  template: `
  <nav class="navbar navbar-inverse">
        <div class="container-fluid">
            <div class="navbar-header">
                <span class="navbar-brand" id="projectName" href="#">Delphi Project</span>
            </div>
            <ul class="nav navbar-nav">

                <li><a [routerLink]="['home']">Home</a></li>
                <li><a [routerLink]="['contacts']">Contacts</a></li>
                <li><a [routerLink]="['catalog']">Catalog</a></li>
                
          <li><a  data-toggle="modal" data-target="#myModalHorizontal">Auth for admin</a></li>  
          
          <li id="auth" style="visibility:hidden" ><a  [routerLink]="['admin']">ADMIN</a></li>
          <li id="logout" style="visibility:hidden" ><a  (click)="logout()" [routerLink]="['home']">LOGOUT</a></li>

       
          <li  id="login_button"><a  data-toggle="modal" data-target="#login">Login</a></li>
          <li  id="user_logout" style="visibility:hidden"><a  data-toggle="modal"  (click)="logoutUser()" [routerLink]="['home']">Logout</a></li>
          <li  id="myCabinet" style="visibility:hidden"><a  data-toggle="modal" href="userCabinet/{{this.identifier}}">{{this.userName}}</a></li>

          <li  id="register_button"><a  data-toggle="modal" data-target="#register">Register</a></li>

          <li><a  data-toggle="modal" data-target="#basket_goods"><i  id="basket" class="fa fa-2x fa-shopping-basket" aria-hidden="true" style="color:white"></i></a></li>
        

          <!--button type="button" id="login_button"  data-toggle="modal" data-target="#login">Login</button>
          <button type="button" id="user_logout"  data-toggle="modal" style="visibility:hidden" (click)="logoutUser()" [routerLink]="['home']">Logout</button>
          <button type="button" id="myCabinet"  data-toggle="modal" style="visibility:hidden" [routerLink]="['userCabinet/',this.identifier]">{{this.userName}}</button>


          <button type="button" id="register_button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#register">Register</button>
          <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#basket_goods"><i  id="basket" class="fa fa-2x fa-shopping-basket" aria-hidden="true" style="color:black"></i></button-->
          



<div class="modal fade" id="basket_goods" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Корзина</h4>
        </div>
           <div *ngFor="let item of goods">
           <div *ngFor="let storageItems of this.storageNames">
        <div *ngIf="item.name == storageItems">
            <div class="good_description">
                <img src={{item.path}} style="width:80px; height:120px">
                {{item.name}}
                 
                <p>Цена: {{item.price}}</p>
               
            </div>
           <input type="number" placeholder="Amount" [(ngModel)]="item.quantity"/>
           <button class="btn btn-default" (click)="deleteOrder(item)">Удалить из корзины</button>
        </div>
        </div>

    </div>
     <button class="btn btn-info" (click)="getTotal()">Общая сумма</button>
     <div style="float:right;"><h4>{{this.sum}}</h4></div>
        <div class="modal-body">
          <form class="form-horizontal" role="form" >
          <h4 style="text-align:center" class="modal-title">Оформить заказ</h4>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Имя</label>
                        <div class="col-sm-10">
                            <input type="text" name="name_from_basket" class="form-control" id="name_from_basket" placeholder="Имя" [(ngModel)]="name_from_basket"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Адресс</label>
                        <div class="col-sm-10">
                            <input type="adress" name="adress" class="form-control" id="adress" placeholder="Адресс" [(ngModel)]="adress"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Телефон</label>
                        <div class="col-sm-10">
                            <input type="telephone" name="telephone_order" class="form-control" id="telephone_order" placeholder="Telephone" [(ngModel)]="telephone_order"/>
                        </div>
                    </div>
                
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">

                            <button type="submit" data-dismiss="modal" class="btn btn-default" id="addOrder" (click)="addOrder()">Отправить заявку</button>        

                        </div>
                    </div>
                </form>
        </div>
        <div class="modal-footer">
        <br>
          <button type="button" class="btn btn-default" id="button_close" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>




<div class="modal fade" id="login" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Login</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" role="form" >
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="login_name" class="form-control" id="login_name" placeholder="Name" [(ngModel)]="login_name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="login_password" class="form-control" id="login_password" placeholder="Password" [(ngModel)]="login_password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">

                            <button type="submit" data-dismiss="modal" class="btn btn-default" (click)="checkUser()">Sign in</button>        

                        </div>
                    </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="register" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title">Register</h4>
        </div>
        <div class="modal-body">
          <form class="form-horizontal" role="form"   method="POST" action="http://localhost:3002/api/users">
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="register_name" class="form-control" id="register_name" placeholder="Name" [(ngModel)]="register_name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Surname</label>
                        <div class="col-sm-10">
                            <input type="text" name="surname" class="form-control" id="surname" placeholder="Surname" [(ngModel)]="surname"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" [(ngModel)]="email"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Telephone</label>
                        <div class="col-sm-10">
                            <input type="number" name="telephone" class="form-control" id="telephone" placeholder="Telephone" [(ngModel)]="telephone"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="register_password" class="form-control" id="register_password" placeholder="Password" [(ngModel)]="register_password"/>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">

                            <button type="submit"  data-dismiss="modal" class="btn btn-default" (click)="addUser()">Register</button>        

                        </div>
                    </div>
                </form>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>




<!-- Modal -->
<div class="modal fade" id="myModalHorizontal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <!-- Modal Header -->
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                       <span aria-hidden="true">&times;</span>
                       <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="myModalLabel">
                    Admin Panel
                </h4>
            </div>

            <!-- Modal Body -->
            <div class="modal-body">

                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputEmail3">Name</label>
                        <div class="col-sm-10">
                            <input type="text" name="name" class="form-control" id="inputEmail3" placeholder="Name" [(ngModel)]="name"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="inputPassword3">Password</label>
                        <div class="col-sm-10">
                            <input type="password" name="password" class="form-control" id="inputPassword3" placeholder="Password" [(ngModel)]="password"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                        
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">

                            <button type="submit" data-dismiss="modal" class="btn btn-default"  (click)="checkAdmin()" >Sign in</button>        

                        </div>
                    </div>
                </form>






            </div>

            <!-- Modal Footer -->
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" >
                            Close
                </button>
                
            </div>
        </div>
    </div>
</div>

                <!--
                <li><a href="/home">Home</a></li>
                <li><a href="/contacts">Contacts</a></li>-->


            </ul>
        </div>
    </nav>
    
    <router-outlet></router-outlet>`
  
})

export class RootComponent {
    good: any;
    key: string;
    data: any;
    telephone_order: any;
    address: any;
    name_from_basket: any;
    quantity: any;



constructor(private http: Http, private services:Services, private router: Router) { 
    
}
private single_user: any;
private identifier:any;
private userName:any;
private admin: any;
private sum : number;
name : string;
password:string;
users: string;
register_name: string;
register_password:string;
surname:string;
email:string;
telephone:string;
login_name:string;
login_password:string;
goods: any;
amount: number;
storageNames= [];



 ngOnInit(): void {
   this.services.getGoods()
    .subscribe(goods=> {
    this.goods = goods;
     console.log(this.goods);
     
      });

for(var i = 0; i<localStorage.length;i++){
    
         //if((localStorage.getItem(localStorage.key(i)))==(this.goods[i].name)){
             //if(this.goods[0].name == localStorage.getItem(localStorage.key(i)))
        this.storageNames[i] = localStorage.getItem(localStorage.key(i));
         //}
    
}
console.log(this.storageNames);   


      
      if(localStorage.getItem("name")){
          document.getElementById("auth").style.visibility = "visible";
           document.getElementById("logout").style.visibility = "visible";
      
      }
      if(localStorage.getItem("username")){
            document.getElementById("login_button").style.visibility = "hidden";
            document.getElementById("register_button").style.visibility = "hidden";
            document.getElementById("user_logout").style.visibility = "visible";
            document.getElementById("myCabinet").style.visibility = "visible";
                  }
this.identifier = localStorage.getItem('user_id');
this.userName = localStorage.getItem("username");

 console.log(window.location.toString());
    if((window.location.toString()=="http://localhost:3000/admin")&&(!localStorage.getItem("name"))){
            this.router.navigate(['home']);
    }

  }
deleteOrder(item){

    for(var i = 0; i<localStorage.length;i++){
    
         if((localStorage.getItem(localStorage.key(i)))==item.name){
            localStorage.removeItem(localStorage.key(i));
}
    
    console.log(item.name);
    window.location.reload();
}
}
getTotal(){
    this.sum = 0;
    
    for (var i = 0; i < this.goods.length; i++) {
         console.log(localStorage.getItem(this.goods[i]._id));

      
        if(this.goods[i].name==localStorage.getItem((this.goods[i]._id))){
            this.sum = this.sum + this.goods[i].quantity * parseInt(this.goods[i].price);
        //this.sum +=  this.goods[i].price;
    
    }else if(this.goods[i].name==null){
        console.log("NULL")
        break;
    }
   
}
   //console.log(this.sum);
}


logout(){
    
    if(localStorage.getItem("name")){
        //localStorage.clear();
        localStorage.removeItem("name");
        localStorage.removeItem("token");
        window.location.reload();
    
    }
}
logoutUser(){
    
    if(localStorage.getItem("username")){
        //localStorage.clear();
        localStorage.removeItem("username");
        localStorage.removeItem("user_id");
        for (var i = 0; i < this.goods.length; i++) {
    localStorage.removeItem(this.goods[i]._id);;
}
        
        window.location.reload();
    
    }
}
addOrder(){
    
    var order = {
        name:this.name_from_basket,
        adress:this.address,
        telephone:this.telephone_order,
        goodName:this.storageNames,
        userId:this.identifier,
        totalSum:this.sum
        
    }



    this.name_from_basket="",
    this.address="",
    this.telephone_order=""
    console.log(order);
    this.services.addOrder(order)
    .subscribe(orders=>{
      
      console.log(orders);
    });
  
}
addUser(){

    var user = {
        name:this.register_name,
        password:Md5.hashStr(this.register_password),
        surname:this.surname,
        email:this.email,
        telephone:this.telephone
    }
    
    this.register_name="",
    this.register_password="",
    this.surname="",
    this.email="",
    this.telephone=""
    
    this.services.addUser(user)
    .subscribe(users=>{
      console.log(users);
    });
}

checkUser(){
    
    var user = {
        name:this.login_name,
        password:Md5.hashStr(this.login_password)
    }
    
    const localName = this.login_name;
    this.login_name="",
    this.login_password=""
    //console.log(this.login_name + " " + this.login_password);
    
    let headers = new Headers({
    'Content-Type': 'application/json' 
});
    let options = new RequestOptions({ headers: headers });
return this.http.post('http://localhost:3002/api/user_check', user, options)
   .map(res=>res.json())
   .subscribe(data => {

            this.single_user = data;
            console.log(this.single_user[0]._id);
              if(this.single_user[0].name==localName){
                  
                  localStorage.setItem('username', localName);
                  localStorage.setItem('user_id',this.single_user[0]._id);
                //   this.identifier = localStorage.getItem('user_id');
                  console.log("OOOKK");
                    
                  this.router.navigate(['userCabinet/',this.single_user[0]._id]);
                  window.location.reload();

              } else {
              console.log("NOOOOO");
              this.router.navigate(['home']);}
                
            },
            err => {
              console.log("ERROR!: ", err);
            }
        );
  
}

checkAdmin(){

  var admin = {
      name : this.name,
      password: this.password

  }

let formData = JSON.stringify(admin);
  console.log(formData);

  let headers = new Headers({
    'Content-Type': 'application/json'  
});
let options = new RequestOptions({ headers: headers });
   
   return this.http.post('http://localhost:3002/api/admin', formData, options)
   .map(res=>res.json())
   .subscribe(data => {
             this.data=data;
             console.log(this.data);
              if(this.data['status']==true){
                  localStorage.setItem('token',this.data['token']); 
                  localStorage.setItem('name', 'admin');
                  this.router.navigate(['admin']);
                  window.location.reload();

              } else {this.router.navigate(['home']);
                console.log(data);}
            },
            err => {
              console.log("ERROR!: ", err);
             }
        );


}

}



const APP_ROUTES:Routes=[
  // { path: '', component: HelloComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full'},
  { path: 'home', component: HelloComponent },
  { path: 'contacts', component: Contacts },
  { path: 'catalog', component: Catalog },
  { path: 'goods/:id', component: Goods },
  { path: 'admin', component: Adminform },
  { path: 'userCabinet/:id', component: UserCabinet}

]; 

export const routing = RouterModule.forRoot(APP_ROUTES);




