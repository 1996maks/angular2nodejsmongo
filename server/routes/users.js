var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://Maxik:1996maks@ds117311.mlab.com:17311/angular2nodejs', ['users']);



// Get all users
router.get('/users', function(req, res, next) {



    db.users.find(function(err, users) {
        if (err) {
            res.send(err);
        }
        res.json(users);
    });

});

router.get('/users/:id', function(req, res, next) {
    db.users.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(err, user) {
        if (err) {
            res.send(err);
        }
        res.json(user);
    });

});


router.post('/users', function(req, res, next) {

    var user = req.body;


    if (!user.name) {
        res.status(400);
        res.json({
            "error": "Bad data"
        });

    } else {

        db.users.save(user, function(err, good) {
            if (err) {
                res.send(err);
            }

            res.json(user);
        });
    }

});


//Update Task
router.put('/users/:id', function(req, res, next) {
    // var good = req.body;
    // db.update({ "_id": good.id }, { $set: { "name": good.changedName } });
    var user = req.body;
    var updTask = {};
    if (user.name) {
        updTask.name = user.name;
        updTask.password = user.password;
        updTask.surname = user.surname;
        updTask.email = user.email;
        updTask.telephone = user.telephone;

    }
    if (!user.name) {
        res.status(400);
        res.json({
            "error": "Bad Data"

        });

    } else {
        db.users.update({ _id: mongojs.ObjectId(req.params.id) }, updTask, {}, function(err, user) {
            if (err) {
                res.send(err);
            }
            res.json(user);
        });
    }




});

module.exports = router;