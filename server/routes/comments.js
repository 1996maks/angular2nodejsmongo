var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://Maxik:1996maks@ds117311.mlab.com:17311/angular2nodejs', ['comments']);



// Get all comments
router.get('/comments', function(req, res, next) {
    db.comments.find(function(err, comments) {
        if (err) {
            res.send(err);
        }
        res.json(comments);
    });
    //res.send('Tasks page');
});


// Get Single Comment
router.get('/comments/:id', function(req, res, next) {
    db.comments.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(err, comment) {
        if (err) {
            res.send(err);
        }
        res.json(comment);
    });

});

//Save Task
router.post('/comments', function(req, res, next) {
    var comment = req.body;
    if (!comment.text) {

        res.status(400);
        res.json({
            "error": "Bad data"
        });
    } else {

        db.comments.save(comment, function(err, comment) {

            if (err) {
                res.send(err);
            }
            res.json(comment);
        });
    }
});

//Delete Task
router.delete('/comments/:id', function(req, res, next) {

    db.comments.remove({ '_id': mongojs.ObjectId(req.params.id) }, function(err, comment) {

        if (err) {

            res.send(err);
        } else {

            res.json(comment);
        }
    });

});

//Update Task
router.put('/comments/:id', function(req, res, next) {
    var comment = req.body;
    var updTask = {};
    if (comment.text) {
        updTask.text = comment.text;
    }
    if (!comment.text) {
        res.status(400);
        res.json({
            "error": "Bad Data"
        });
    } else {
        db.comments.update({ _id: mongojs.ObjectId(req.params.id) }, updTask, {}, function(err, comment) {
            if (err) {
                res.send(err);
            }
            res.json(comment);
        });
    }


});


module.exports = router;