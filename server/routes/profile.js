var express = require('express');
var router = express.Router();
var multer = require('multer');
var bodyParser = require('body-parser');
var fs = require('fs');
var gm = require('gm').subClass({ imageMagick: true });

var Jimp = require("jimp");

var Storage = multer.diskStorage({
    destination: function(req, file, callback) {
        callback(null, "./public/images");
    },
    filename: function(req, file, callback) {
        callback(null, file.originalname);
    }
});
var upload = multer({ storage: Storage }).single('good_picture');


router.post("/profile", function(req, res) {
    upload(req, res, function(err) {
        if (err) {
            return res.jsonp("Something went wrong!");
        } else {

            Jimp.read(req.file.path, function(err, image) {
                if (err) throw err;
                image.resize(50, 50) // resize 
                    .quality(60) // set JPEG quality 
                    //.greyscale() // set greyscale 
                    .write("./public/images/resize/" + req.file.originalname); // save 
            });

            return res.jsonp("File uploaded sucessfully!");

        }

    });
});



module.exports = router;