var express = require('express');
var router = express.Router();
var mongojs = require('mongojs');
var db = mongojs('mongodb://Maxik:1996maks@ds117311.mlab.com:17311/angular2nodejs', ['goods']);
const crypto = require('crypto');
const hash = crypto.createHash('sha256');
var secret = "secret";
//var date = new Date();
//var time = date.getHours() + 1 + ':' + date.getMinutes();

hash.update(secret);
var token = hash.digest('hex');

// Get all comments
router.get('/goods', function(req, res, next) {
    db.goods.find(function(err, goods) {
        if (err) {
            res.send(err);
        }
        res.json(goods);
    });
    //res.send('Tasks page');
});


// Get Single Comment
router.get('/goods/:id', function(req, res, next) {
    db.goods.findOne({ _id: mongojs.ObjectId(req.params.id) }, function(err, good) {
        if (err) {
            res.send(err);
        }
        res.json(good);
    });

});

//Save Task
router.post('/goods', function(req, res, next) {
    //  if (good.path) {
    //             var file = good.path;
    //             filename = file.name;
    //             file.mv("public" + filename, function(err) {
    //                 if (err) {
    //                     console.log(err);
    //                     res.send('error ocured');
    //                 } else {
    //                     res.send("Done");
    //                 }
    //             });
    //         }
    var good = req.body;


    if (!good.name) {
        // if (req.get("My-Token") == token) {
        res.status(400);
        res.json({
            "error": "Bad data"
        });

    } else {

        db.goods.save(good, function(err, good) {
            if (err) {
                res.send(err);
            }
            console.log(token);
            console.log(req.get("My-Token"));
            if (req.get("My-Token") == token) {
                res.json(good);
            }
        });
    }

});

//Delete Task
router.delete('/goods/:id', function(req, res, next) {

    db.goods.remove({ _id: mongojs.ObjectId(req.params.id) }, function(err, good) {
        if (err) {
            res.send(err);
        }
        res.json(good);
    });

});

//Update Task
router.put('/goods/:id', function(req, res, next) {
    // var good = req.body;
    // db.update({ "_id": good.id }, { $set: { "name": good.changedName } });
    var good = req.body;
    var updTask = {};
    if (good.changedName) {
        updTask.name = good.changedName;
        updTask.diagonal = good.diagonal;
        updTask.razresheniye = good.razresheniye;
        updTask.memory = good.memory;
        updTask.camera = good.camera;
        updTask.processor = good.processor;
        updTask.description = good.description;
        updTask.price = good.price;
        updTask.path = good.path_images;
    }
    if (!good.changedName) {
        res.status(400);
        res.json({
            "error": "Bad Data"

        });

    } else {
        db.goods.update({ _id: mongojs.ObjectId(req.params.id) }, updTask, {}, function(err, good) {
            if (err) {
                res.send(err);
            }
            res.json(good);
        });
    }


});


module.exports = router;