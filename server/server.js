var express = require('express');
var path = require('path');
var bodyParser = require('body-parser');
// var bson = require('bson');

//var multer = require('multer');

//var upload = require("express-fileupload");


var index = require('./routes/index');
var comments = require('./routes/comments');
var goods = require('./routes/goods');
var admin = require('./routes/admin');
var profile = require('./routes/profile');
var users = require('./routes/users');
var user_check = require('./routes/user_check');
var orders = require('./routes/orders');

var port = 3002;
var app = express();


app.engine('html', require('ejs').renderFile);



app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, My-Token");
    res.header("Access-Control-Allow-Methods", "PUT,DELETE");

    next();
});


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));

app.use(express.static('public'));
//app.use(express.static(path.join('images', './public')));


app.use('/api', index);
app.use('/api', comments);
app.use('/api', goods);
app.use('/api', admin);
app.use('/api', profile);
app.use('/api', users);
app.use('/api', user_check);
app.use('/api', orders);


app.listen(port, function() {
    // var host = 'localhost';
    // var port = server.address().port;

    console.log('App listening at ' + port);
});

module.exports = app;